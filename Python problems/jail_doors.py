def jail_problem(limit: int) -> list :
    the_num_of_doors = [(i*i) for i in range(1, int(limit**0.5)+1)]
    return(the_num_of_doors)

print(jail_problem(100))
