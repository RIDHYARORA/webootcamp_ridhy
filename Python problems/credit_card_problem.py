def credit_card(card_number: int) -> int:
    total = 0
    card_number = str(card_number)
    for i, digit in enumerate(reversed(card_number)):
        if i % 2 == 0:
            total += int(digit)
        else:
            doubled = int(digit) * 2
            total += (doubled // 10) + (doubled % 10)
    return total % 10
credit_card(401288888888188)
