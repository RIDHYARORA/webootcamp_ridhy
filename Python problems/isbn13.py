def check_isbn_13(num: int) -> bool:
    isbn_str = str(num)
    if len(isbn_str) != 13:
        return False

    total = 0
    for i, digit in enumerate(isbn_str):
        digit = int(digit)
        if i % 2 == 0:
            total += digit
        else:
            total += digit * 3

    return total / 11
print(check_isbn_13(9780306406157))
