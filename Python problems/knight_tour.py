def knight_steps(start):
    r, c = 8, 8
    steps = 0
    chessboard = [[0 for i in range (r)]for i in range (c)]
    chessboard[start[0]][start[1]] = 1
    visited = set()
    visited.add((start[0], start[1]))

    moves = [(2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1)]

    def abc(x, y):
        nonlocal steps
        for dx, dy in moves:
            new_x, new_y = x + dx, y + dy
            if 0 <= new_x < r and 0 <= new_y < c and chessboard[new_x][new_y] == 0:
                chessboard[new_x][new_y] = 1
                visited.add((new_x, new_y))
                steps += 1
                abc(new_x, new_y)

    abc(start[0], start[1])
    return steps
start = (0, 0)
print(knight_steps(start))
    
